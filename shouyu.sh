#!/bin/sh
set -eux

-abort() {
	printf '%s\n' "$*" >&2
	exit 1
}

-copy() {
	# bsdtar-specific
	tar -cPf - -- "$@" | tar -xPf - -C "$mnt" --chroot
}
-add() {
	while	case $1 in
		(/*)	set -- "${1#/}" ;;
		(*/)	set -- "${1%/}" ;;
		(*)	! : ;;
		esac
	do { }; done

	cp -a "$@" "$mnt/$__dst/"
}
-put() {
	# bsdtar-specific
	__dst=$1; shift
	while	case $__dst in
		(/*)	__dst=${__dst#/} ;;
		(*/)	__dst=${__dst%/} ;;
		(*)	! __dst=$mnt/$__dst ;;
		esac
	do { }; done
	mkdir -p "${__dst%/*}"

	{
		case $1 in
		(0*)	chmod "$1" "$__dst"; shift ;;
		esac

		"$@"
	} > "$__dst"
}
-run()	chroot "$mnt" "$@"

-link() {
	[ ${3+1} ] && -abort -link: too many arguments
	set -- "${1%/}" "$mnt/${2#/}"
	mkdir -p "${2%/*}"
	ln -s "$1" "$2"
}
-pkg() {
	env ASSUME_ALWAYS_YES=yes chroot "$mnt" pkg "$@"
	rm "$mnt/var/db/pkg/repo-"*
}

__main() {
	case $1 in
	(*/*)	. "$1" ;;
	(*)	. "./$1" ;;
	esac
	shift


	for cmd in "$@"; do
		case $cmd in
		(*=*)	export "$cmd" ;;
		(*)	"__$cmd" ;;
		esac
	done
}

_zfs_age() {
	zfs list -r "$1" 2>/dev/null || return 0
	zfs list -r "$2" 2>/dev/null && zfs rename "$2" "$1/p"
	zfs rename -u "$1" "$2"
}

__build() {
	: pool="$pool"
	: img="$img"
	: tag="${tag=tip}"
	: src="${src=`freebsd-version | sed 's/-.*//'`}"
	: dst="${dst=$img}"

	case $src in (*/*) ;; (*) src=$pool/i/$src ;; esac
	case $dst in (*/*) ;; (*) dst=$pool/i/$dst ;; esac
	mnt=${2-/media/$$}
	old=$dst-old

	_zfs_age "$dst" "$old"
	zfs clone -o readonly=off "$src@$tag" "$dst"
	mkdir "$mnt"
	mount -t zfs "$dst" "$mnt"

	build

	umount "$mnt"
	rmdir "$mnt"
	zfs snap "$dst@$tag"
	zfs inherit readonly "$dst"
}

__replace() {
	: pool="$pool"
	: img="$img"
	: tag="${tag=tip}"
	: dst="${dst=$img}"
	: run="${run=$img}"
	: svc="${svc=/run/s6/jail:$img}"

	case $dst in (*/*) ;; (*) dst=$pool/i/$dst ;; esac
	case $run in (*/*) ;; (*) run=$pool/j/$run ;; esac
	old=$run-old
	new=$run-new

	_zfs_age "$run" "$old"
	zfs clone "$dst@$tag" "$new"
	zfs rename -u "$new" "$run"
}

__displace() {
	: pool="$pool"
	: img="$img"
	: tag="${tag=tip}"
	: dst="${dst=$img}"
	: run="${run=$img}"
	: svc="${svc=/run/s6/jail:$img}"

	case $dst in (*/*) ;; (*) dst=$pool/i/$dst ;; esac
	case $run in (*/*) ;; (*) run=$pool/j/$run ;; esac
	old=$run-old

	s6-svc -d -wD "$svc"
	zfs umount "$run" || zfs umount -f "$run" || { }

	_zfs_age "$run" "$old"
	zfs clone "$dst@$tag" "$run"
	s6-svc -u "$svc"
}

__main "$@"
# ex: se isk+=- ts=8 :
